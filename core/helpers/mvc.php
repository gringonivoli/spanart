<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
* Funciones varias para ayudar a los objetos del mvc
* 
* @author Maxi Nivoli <maxi.nivoli@gmx.com>
* @package Woper Engine
* @subpackage Helpers
*/

if(!function_exists('build_controller_name')){
   /**
 	* Build Controller Name
 	* 
 	* Genera un nombre correcto para el controller.
 	* Generalmente la forma en como se debe llamar la clase.
 	*
 	* @author Maxi Nivoli <maxi.nivoli@gmx.com>
 	* @package Woper Engine
 	* @subpackage Helpers
 	* @access public
 	* @param string
 	*/	
	function build_controller_name($controller){
		return $controller = $controller."Controller";
	}
	
}

if(!function_exists('build_view_name')){
   /**
 	* Build View Name
 	* 
 	* Genera un nombre correcto para la view.
 	* Generalmente la forma en como se debe llamar la clase.
 	*
 	* @author Maxi Nivoli <maxi.nivoli@gmx.com>
 	* @package Woper Engine
 	* @subpackage Helpers
 	* @access public
 	* @param string
 	*/
	function build_view_name($str){
		return $view = $str."View";
	}

}

/* End of file mvc.php */
/* Location: ./core/helpers/mvc.php */