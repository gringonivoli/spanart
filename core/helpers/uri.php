<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
* Funciones para ayudar con los enlaces
* No estan cargadas por defecto, para su uso 
* importalas cuando se crea necesario o desde 
* el comienzo de la ejecucion en el archivo de 
* configuracion autoload.php
* 
* @author Maxi Nivoli <maxi.nivoli@gmx.com>
* @package Woper Engine
* @subpackage Helpers
*/

if(!function_exists('base_uri')){
   /**
 	* Base Uri
 	* 
 	* Genera la uri base de la app.
 	*	
	* @author Maxi Nivoli <maxi.nivoli@gmx.com>
 	* @package Woper Engine
 	* @subpackage Helpers
 	* @access public
 	* @return string
 	*/	
	function base_uri(){
 		$base_uri = WEB_PATH;
		$base_uri = "/".rtrim($base_uri,'/');
		return $base_uri;
	}
	
}

/* End of file uri.php */
/* Location: ./core/helpers/uri.php */