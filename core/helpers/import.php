<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Helper para la importacion de ficheros.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage Helpers
 */


if(!function_exists('import')){
   /**
 	* Import
 	* 
 	* Importa los ficheros pasados, si no 
 	* existen se producira un error.
	* ej.: import("core.helpers.mvc")
 	*
 	* @author Maxi Nivoli <maxi.nivoli@gmx.com>
 	* @package Woper Engine
 	* @subpackage Helpers
 	* @access public
 	* @param string
 	*/	
	function import($str = ''){
		$file = str_replace('.', '/', $str);
		require_once "{$file}.php";
	}

}

if(!function_exists('secure_import')){
   /**
	* Secure Import
 	* 
 	* Importa los ficheros pasados pero se asegura 
 	* de que existan, si no existen no importa nada.
 	* ej.: secure_import("core.helpers.mvc")
 	*
 	* @author Maxi Nivoli <maxi.nivoli@gmx.com>
 	* @package Woper Engine
 	* @subpackage Helpers
 	* @access public
 	* @param string
 	* @return boolean
 	*/	
	function secure_import($str = ''){
		$file = str_replace('.', '/', $str);
		$file = "{$file}.php";
		$check = false;
		if(file_exists($file)){
			require_once "{$file}";
			$check = true;
		}
		return $check;
	}
	
}

/* End of file import.php */
/* Location: ./core/helpers/import.php */