<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase que de acuerdo a la ruta, argumentos (de la peticion) y configuracion 
 * prepara a quien y adonde hay que llamar.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage MVC
 */

class Armador{
	private $module;
	private $controller;
	private $method;
	private $args;
	
	private $controllerName;
	private $rutaController;
	
	private $request;
	private $error;

	/**
	 * Constructor.
	 * 
	 * @param RequestInterface.
	 */
	public function __construct(RequestInterface $request){
		$this->request = $request;
		$this->error   = false;
	}

	/**
	 * Analizar Peticion.
	 *
	 * Analiza el array con los miembros de la request y 
	 * llama a la funcion set_peticiones.
	 *
	 * @access public
	 */
	public function analizarPeticion(){
		$peticion 		 = $this->request->getUriArray();
		$numeroMiembros = count($peticion);
		$this->setPeticion($numeroMiembros,$peticion);
	}

	/**
	 * Set Peticiones.
	 * 
	 * De acuerdo al tipo de peticion y a su numero de miembros 
	 * llama a la funcion correspondiente.
	 * 
	 * @access private
	 * @param int   (numero de miembros de la peticion)
	 * @param array (array que contiene los miembros de la peticion)
	 */
	private function setPeticion($numeroMiembros, $peticion){
		if($numeroMiembros == 0){
			$this->setDefaultInit();
		}elseif($numeroMiembros >= 3){
			$this->setCustomPeticion($peticion);
		}elseif($numeroMiembros == 2){
			array_push($peticion,"index");
			$this->setCustomPeticion($peticion);
		}

	}

	/**
	* Set Default Init
	*
	* LLamada por set_peticion, setea module, controller y 
	* method con los valores por defecto especificados en 
	* el archivo de configuracion.
	*
	* @access private
	*/
	private function setDefaultInit(){
			$this->module 	   = INIT_MODULE;
			$this->controller  = INIT_CONTROLLER;
			$this->method      = INIT_METHOD;
	}

	/**
	 * Set Custom Peticion
	 *
	 * Llamada por set_peticiones, setea module, controller y 
	 * method con los miembros del array peticion.
	 *
	 * @access private
	 * @param array (array que contiene los miembros de la peticion)
	 */
	private function setCustomPeticion($peticion){
		list($this->module, $this->controller, $this->method) = $peticion;
		$this->controller = ucwords($this->controller);
	}

	/**
	 * Set Args
	 *
	 * Setea args con el array de argumentos devuelto por el 
	 * objeto request.
	 *
	 * @access private
	 */
	public function setArgs(){
			$this->args = $this->request->getArgsArray();
	}

	/**
	 * Prepare Controller Name
	 *
	 * Haciendo uso de la funcion global build_controller_name 
	 * setea a controllerName.
	 *
	 * @access private
	 */
	private function prepareControllerName(){
		$this->controllerName = build_controller_name($this->controller);
	}

	/**
	 * Prepare Ruta Controller
	 *
	 * Arma la ruta en donde se encuentra el controller 
	 * de la peticion.
	 *
	 * @access private
	 * @throws Exception (se le pasa el mensaje de error definido en el archivo de configuracion)
	 */
	private function prepareRutaController(){
		$this->rutaController = APP_PATH."modules/{$this->module}/controllers/{$this->controller}";
		if(!(file_exists($this->rutaController.".php"))){
			throw new Exception(ERROR_MSG_NO_FILE);
		}
	}

	/**
	 * Verificar Ruta
	 *
	 * Verifica si la ruta del controller es valida.
	 *
	 * @access public
	 * @return boolean (true si es valida, false si no lo es)
	 */
	public function verificarRuta(){
		try{
			$this->prepareRutaController();
		}catch(Exception $e){
			$this->error = $e->getMessage();
		}
		return $this->error;
	}

	/**
	 * Get Controller Name
	 *
	 * Devuelve el nombre del controller. 
	 *
	 * @access public
	 * @return string (Nombre de la clase controller)
	 */
	public function getControllerName(){
		$this->prepareControllerName();
		return $this->controllerName;
	}

	/**
	 * Get Ruta Controller
	 *
	 * Devuelve la ruta del controller.
	 *
	 * @access public
	 * @return string (ruta en donde se encuentra el fichero del controlador)
	 */
	public function getRutaController(){
		return $this->rutaController;
	}

	/**
	 * Get Resource
	 *
	 * Devuelve el metodo a ejecutar por el controller.
	 *
	 * @access public
	 * @return string
	 */
	public function getMethod(){
		return $this->method;
	}

	/**
	 * Get Args
	 *
	 * Devuelve los argumentos para el metodo del controller.
	 *
	 * @access public
	 * @return array
	 */
	public function getArgs(){
		return $this->args;
	}

	/**
	 * Get Controller
	 *
	 * Devuelve el controller especificado en la peticion.
	 *
	 * @access public
	 * @return string
	 */
	public function getController(){
		return $this->controller;
	}

	/**
	 * Get Module
	 *
	 * Devuelve el module especificado en la peticion.
	 *
	 * @access public
	 * @return strng
	 */
	public function getModule(){
		return $this->module;
	}
}
// END Armador Class

/* End of file Armador.php */
/* Location: ./core/mvc/Armador.php */