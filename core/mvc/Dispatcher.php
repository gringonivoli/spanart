<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase que carga y llama al controlador que corresponda.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage MVC
 */

class Dispatcher{
	private $rutaController;
	private $controllerName;
	private $method;
	private $args;
	private $controller;
	private $module;

	/**
	 * Contructor
	 *
	 * Setea las variables privadas para poder trabajar
	 *
	 * @param string
	 * @param string
	 * @param string
	 * @param array
	 * @param string
	 * @param string
	 */
	public function __construct($rc, $cn, $r, $a, $c, $module){
		$this->rutaController  = $rc;
		$this->controllerName  = $cn;
		$this->method 	       = $r;
		$this->args 		   = $a;
		$this->controller 	   = $c;
		$this->module 		   = $module;
	}

	/**
	 * Cargar Controller
	 *
	 * Importa el fichero del controller para poder usarlo.
	 *
	 * @access private
	 */
	private function cargarController(){
		import("{$this->rutaController}");
	}

	/**
	 * Llamar Controller
	 *
	 * Si existe la clase del controller, el mismo se crea, 
	 * si no, se llama al error.
	 *
	 * @access public
	 */
	public function llamarController(){
		$this->cargarController();
		if(class_exists($this->controllerName)){
			$controllerObj = new $this->controllerName($this->method, $this->args, $this->controller, $this->module);
		}else{
			$this->llamarError();
		}	
	}

	/**
	 * Llamar Error
	 *
	 * Llama al metodo estatico shto_error de la clase ErrorHandler.
	 *
	 * @access private
	 */
	private function llamarError(){
		ErrorHandler::shootError(ERROR_ROUTING, ERROR_MODULE, ERROR_CONTROLLER, ERROR_METHOD, ERROR_MSG_NO_CLASS);
	}
}
// END Dispatcher Class

/* End of file Dispatcher.php */
/* Location: ./core/mvc/Dispatcher.php */