<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase abstracta que deberian extender los controllers de la aplicacion.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage MVC
 */

abstract class Controller{

	protected $view;
	protected $model;

	/**
	 * Constructor
	 *
	 * @param string (metodo a ejecutar en el controller)
	 * @param array  (los argumentos del metodo)
	 * @param strng  (nombre del controller como viene en la peticion, primera en mayuscula)
	 * @param string (modulo al que pertenece el controller)
	 */
	public function __construct($method = '', $args=array(), $controller = '', $module = ''){
		$this->set($module, $controller, $method);
		call_user_func_array(array($this, $method), $args);
	}

	/**
	 * Call
	 *
	 * Aca caen todos los metodos que se intenten llamar y no esten definidos.
	 * por ahora se le pasa parametros solamente para dar soporte al metodo magico, 
	 * mas adelante se pueden usar para pasarselos a ErrorHandler y que este lleve un log 
	 * de los errores o malas intenciones, mas comunes.
	 *
	 * @param string (nombre del metodo)
	 * @param array  (argumentos)
	 */
	public function __call($methodname, $args){
		ErrorHandler::shootError(ERROR_ROUTING, ERROR_MODULE, ERROR_CONTROLLER, ERROR_METHOD, ERROR_MSG_NO_METHOD);
	}

	private function set($module, $controller, $method){
		$vm = $this->importViewModel($module, $controller);
		if(!($vm['view'] === false))
			$this->setView($module, $controller, $method);
		if(!($vm['model'] === false))
			$this->setModel($controller);
	}

	/**
	 * Set View
	 *
	 * Crea la vista correspondiente al controller que se llama 
	 * y se asigna a una propiedad visible al controller.
	 *
	 * @access protected
	 * @param string (nombre del controller para construir el nombre de la view)
	 */
	protected function setView($module, $name, $method){
		$viewObj = build_view_name($name);
		if(class_exists($viewObj)){
			$this->view = new $viewObj($module, $name, $method);
		}
	}

	/**
	 * Set Model
	 *
	 * Crea el modelo correspondiente al controller que se llama 
	 * y se adigna a una propiedad visible al controller.
	 *
	 * @access protected
	 * @param string (nombre del controller que corresponde con el del model)
	 */
	protected function setModel($name){
		$model = $name;
		if(class_exists($model)){
			$this->model = new $model(); 
		}
	}

	/**
	 * Import View Model
	 *
	 * Si existen view y model correspondientes al controller 
	 * se importan para su uso.
	 *
	 * @access protected
	 * @param string (nombre del module al que pertenece el controller)
	 * @param strng  (nombre del fichero del controller que corresponde al del view y al del model)
	 * @return array
	 */
	protected function importViewModel($module, $name){
		$vm['view']  = secure_import(APP_PATH."modules.{$module}.views.{$name}");
		$vm['model'] = secure_import(APP_PATH."modules.{$module}.models.{$name}");
		return $vm;
	}
}
// END Controller Class

/* End of file Controller.php */
/* Location: ./core/mvc/Controller.php */