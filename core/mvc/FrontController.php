<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase que recepta todas las peticiones a la aplicacion.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage MVC
 */

class FrontController{
	
	/**
	 * Start
	 * 
	 * Funcion estatica que inicia el manejo 
	 * de las peticiones.
	 *
	 * @access public
	 */
	public static function start(){
		$request = new Request();
		$armador = new Armador($request);
		$armador->analizarPeticion();
		$errorRuta = $armador->verificarRuta();
		if($errorRuta){
			ErrorHandler::shootError(ERROR_ROUTING, ERROR_MODULE, ERROR_CONTROLLER, ERROR_METHOD, $errorRuta);
		}else{
			$armador->setArgs();
			$disp = new Dispatcher(
								 $armador->getRutaController()
								,$armador->getControllerName()
								,$armador->getMethod()
								,$armador->getArgs()
								,$armador->getController()
								,$armador->getModule());
			$disp->llamarController();
			
		}
	}
}
// END FrontController Class

/* End of file FrontController.php */
/* Location: ./core/mvc/FrontController.php */