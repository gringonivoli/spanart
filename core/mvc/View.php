<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase abstracta que deberian extender las views de la aplicacion.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage MVC
 */

abstract class View{
	
	protected $info = array();

	/**
	 * Constructor
	 *
	 * Los parametros son pasados de forma automatica por el 
	 * Controller del core. Y pueden ser usados para obtener
	 * la ubicacion actual, ej. example -> vidrio -> ver
	 *
	 * @param string 
	 * @param string
	 * @param string  
	 */
	public function __construct($module, $model, $method){
		$this->info['module']   = $module;
		$this->info['model']    = $model;
		$this->info['method']   = $method;
	}

	/**
	 * Show
	 *
	 * Este metodo necesita un argumento o cinco argumentos.
	 * Si se pasa uno el mismo debe contener lo que se va a imprimir por 
	 * pantalla. Si se pasan cuatro el primero debe ser la direccion en donde 
	 * se encuentra el template, el segundo lo que se va a reemplazar en 
	 * el template, el tercero el diccionario con las etiquetas y valores a 
	 * reemplazar en el template y el cuarto si hace falta o no setear las claves 
	 * del diccionario.
	 *
	 * Los parametros que puede recibir son los de los metodos Show In Template y 
	 * Show Render
	 *
	 * @access protected
	 */
	protected function show(){
		$nroArgs = func_num_args();
		switch($nroArgs){
			case 5:
				$this->showInTemplate(func_get_arg(0), func_get_arg(1), func_get_arg(2), func_get_arg(3), func_get_arg(4));
				break;
			case 4:
				$this->showInTemplate(func_get_arg(0), func_get_arg(1), func_get_arg(2), func_get_arg(3));
				break;
			case 3:
				$this->showInTemplate(func_get_arg(0), func_get_arg(1), func_get_arg(2));
				break;			
			case 1:
				$this->showRender(func_get_arg(0));
				break;
			default:
				ErrorHandler::shootError(ERROR_FUNC, ERROR_MODULE, ERROR_CONTROLLER, ERROR_METHOD, "El metodo \"show\" admite 1 o 4 parametros.");
		}
	}

	/**
	 * Show In Template
	 *
	 * Imprime por pantalla un render dentro de un template, la marca de contenido 
	 * para el template es {CONTENIDO}.
	 *
	 * @access private
	 * @param string
	 * @param string
	 * @param array
	 * @param boolean
	 */
	private function showInTemplate($template, $render, $dict = array(), $setDict = false, $addNavPath = false){
		if(file_exists($this->buildTemplatePath($template))){
			if($setDict){
				$this->setDict($dict);
			}
			if($addNavPath){
				$this->addNavPath($dict);
			}
			$dict['{CONTENIDO}'] = $render;
			$template = $this->getTemplate($template);
			print str_replace(array_keys($dict), array_values($dict), $template);
		}else{
			ErrorHandler::shootError(ERROR_FUNC, ERROR_MODULE, ERROR_CONTROLLER, ERROR_METHOD, "No existe el fichero de template: \"{$template}.html\"");
		}
	}

	/**
	 * Show Render
	 *
	 * Imprime por pantalla un render.
	 *
	 * @access private
	 * @param string
	 */
	private function showRender($render){
		if($render == ''){
			ErrorHandler::shootError(ERROR_FUNC, ERROR_MODULE, ERROR_CONTROLLER, ERROR_METHOD, "Esta intentando imprimir el contenido de una variable vacia con el metodo \"showRender\"");
		}else{
			print $render;
		}
	}

	/**
	 * Render Static
	 *
	 * Sustituye un diccionario en un render con marcas de 
	 * sustitucion estaticas.
	 *
	 * @access protected
	 * @param array
	 * @param string
	 * @param boolean
	 * @return string (el html con las marcas estaticas reemplazadas)
	 */
	protected function renderStatic($dict, $render, $setDict = false, $addNavPath = false){
		settype($dict, 'array');
		if($setDict){
			$this->setDict($dict);
		}
		if($addNavPath){
				$this->addNavPath($dict);
		}
		return str_replace(array_keys($dict), array_values($dict), $render);
	}

	/**
	 * Render Regex
	 *
	 * Sustituye un diccionario en un render con marcas de 
	 * sustitucion dinamica.
	 */
	protected function renderRegex($dict, $render, $setDict){
		//VER ESTA QUE ONDERA.
	}

	/**
	 * Add Nav Path
	 *
	 * Agrega al diccionario de reemplazo el path para la navegacion,
	 * esto sirve para no tener ningun problema a la hora de cambiar la 
	 * app a distintos niveles de directorios.
	 *
	 * @access protected
	 * @param array (por referencia)
	 */
	protected function addNavPath(&$dict){
		$dict['{wNAV}'] = base_uri();
	}

	/**
	 * Set Dict
	 * 
	 * Setea el diccionario para usarlo en las sustituciones.
	 *
	 * @access protected
	 * @param array (por referencia)
	 */
	protected function setDict(&$dict){
        settype($dict, 'array');
        if($this->isDepth($dict)){
        	$dict = $this->xarr2Arr($dict); 
        }
        $keys = array_keys($dict);
        $values = array_values($dict);
        foreach($keys as &$key) {
            $key = "{{$key}}";
        }
        $dict = array_combine($keys, $values);
    }

    /**
    * Is Depth
    *
    * Verifica si un array es multidimensional.
    *
    * @access protected
    * @return boolean
    */
    protected function isDepth(array $array){
    	# VER SI ES CONVENIENTE CAMBIAR POR UN WHILE EN TERMINOS DE PERFORMANCE.
    	foreach ($array as $value){
    		if(is_array($value)){    			
    			return true;
    		}
    	}
    	return false;
    }

    /**
     * xarr2Arr
     *
     * Transforma un array de "x" dimensiones en uno de 
     * dos dimensiones.
     *
     * @access protected
     * @param array
     * @return array (el objeto transformado en array, sin importar su profundidad)
     */
    protected function xarr2Arr($dict){
    	$ritit = new RecursiveIteratorIterator(new RecursiveArrayIterator($dict));
		$resut = array();
		foreach ($ritit as $leafValue) {
    		$keys = array();
    		foreach (range(0, $ritit->getDepth()) as $depth) {
        		$keys[] = $ritit->getSubIterator($depth)->key();
    		}
    		$result[ join('.', $keys) ] = $leafValue;
		}
		return $result;
	}

	/**
	 * Get Html
	 *
	 * Devuelve el fichero html indicado, tener en cuenta que 
	 * se debe respetar la estructura de directorios de static/.
	 * Pero dentro del directorio html se pueden crear sub directorios.
	 * ej. del caso $this->getHtml("web/index");
	 *
	 * @access protected
	 * @param string
	 * @return string (el contenido del fichero html convertido en string)
	 */
    protected function getHtml($html){
    	$html = $this->buildHtmlPath($html);
    	return $html = (file_exists($html)) ? file_get_contents($html) : false;
    }

    /**
     * Get Template
     *
     * Devuelve el template requerido para usarlo en 
     * Show In Template. Si se usa afuera el ejemplo es 
     * de la misma forma que el de Get Html.
     *
     * @access protected
     * @param string
     * @return string (el contenido del fichero template convertido en string)
     */
    protected function getTemplate($template){
    	$template = $this->buildTemplatePath($template);
    	return $template = (file_exists($template)) ? file_get_contents($template) : false;
    }

    /**
     * Build Template Path
     *
     * Devuelve la ruta del template pasado, no verifica que exista.
     *
     * @access protected
     * @param string
     * @return string
     */
    protected function buildTemplatePath($template){
    	return STATIC_PATH."html/{$template}.html";
    }

    /**
     * Build Html Path
     *
     * Devuelve la ruta del html pasado, no verifica que exista.
     *
     * @access protected
     * @param string
     * @return string
     */
    protected function buildHtmlPath($html){
    	return $html = STATIC_PATH."html/{$html}.html";
    }
}
// END View Class

/* End of file View.php */
/* Location: ./core/mvc/View.php */