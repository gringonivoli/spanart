<?php
/**
* Interface que define algunas comprobaciones sobre archivos.
* 
* @author Maxi Nivoli
*/
interface ChkFile{
	public function existe($path);
	public function is_readable($path);
	public function is_writable($path);
}


/* End of file chkfile.php */
/* Location: ./core/interface/chkfile.php */