<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
* Interface para que implemente, por lo general, un objeto tipo Request.
* El objeto Armador espera recibir un objeto que implemente es interface.
* 
* @author Maxi Nivoli <maxi.nivoli@gmx.com>
*/

interface RequestInterface{
	public function getUriArray();
	public function getArgsArray();
}

/* End of file Request.php */
/* Location: ./core/interfaces/Request.php */