<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");

interface ConfigInterface{
	public function load();
	public function item();
}