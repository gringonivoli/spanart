<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase que, si es necesario, carga helpers y libs. 
 *
 * Los helpers/libs pueden ser del core y/o definidos por el usuario 
 * en base al archivo de configuracion autoload.php
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage Libs
 */

class Autoload{
	private static $existsHelpers = false;
	private static $existsLibs    = false;
	private static $tryLoad 	   = false;

	/**
	 * Constructor
	 *
	 * Dependiendo de como el metodo public static check seteo
	 * las variables estaticas llama a cargar helpers y/o libs.
	 *
	 * @access public
	 * @param array
	 * @param array
	 */
	public function __construct($helpers, $libs){
		if(self::$existsHelpers) $this->loadHelpers($helpers);
		if(self::$existsLibs) 	  $this->loadLibs($libs);
		if(self::$tryLoad)	      spl_autoload_register(array($this, 'tryLoad'));
	}

	/**
	 * Load Helpers
	 *
	 * Carga los helpers definidos de manera automatica.
	 *
	 * @access private
	 * @param array
	 */
	private function loadHelpers($helpers){
		$this->load($helpers, "helpers");
	}

	/**
	 * Load Libs
	 *
	 * Carga las libs definidas de manera automatica.
	 *
	 * @access private
	 * @param array
	 */
	private function loadLibs($libs){
		$this->load($libs, "libs");
	}

	/**
	 * Load
	 *
	 * Carga los helpers/libs de manera automatica, definidos en el fichero 
	 * de configuracion autoload.
	 * Dependiendo de los argumentos que reciba.
	 *
	 * @access private
	 * @param array
	 * @param string (para saber lo que se busca, si un helper o una lib)
	 */
	private function load($array_names, $dir){
		$app_path  = APP_PATH.$dir;
		$core_path = CORE_PATH.$dir;
		foreach($array_names as $file){
			if($this->checkFile($this->armarRuta($app_path, $file))){
				import("{$app_path}.{$file}");
				//echo "<br>importar: {$app_path}.{$file}<br>";
			}
			if($this->checkFile($this->armarRuta($core_path, $file))){
				import("{$core_path}.{$file}");
				//echo "<br>importar: {$core_path}.{$file}<br>";
			}
		}
	}

	/**
	 * Try Load
	 *
	 * Como ultima instancia, si no se definio la clase/interfaz que se
	 * intenta instanciar, y esta activada probar la carga desde el fichero
	 * de configuracion este metodo hace lo necesario para encontrarla.
	 *
	 * @access private
	 * @param string
	 */
	private function tryLoad($classname){
		$paths['app_libs']     = APP_PATH."libs";
		$paths['app_helpers']  = APP_PATH."helpers";
		$paths['core_libs']    = CORE_PATH."libs";
		$paths['core_helpers'] = CORE_PATH."helpers";

		foreach ($paths as $path) {
			if($this->checkFile($this->armarRuta($path, $classname))){
				import("{$path}.{$classname}");
			}
		}

	}

	/**
	 * Armar Ruta
	 *
	 * Arma la ruta para importar los helpers/libs.
	 *
	 * @access private
	 * @param string
	 * @param string
	 * @return string
	 */
	private function armarRuta($path, $file){
		return "{$path}/{$file}.php";
	}

	/**
	 * Check File
	 *
	 * Comprueba si el archivo existe en la ruta declarada.
	 *
	 * @access private
	 * @param string
	 * @return boolean
	 */
	private function checkFile($ruta){
		$result = true;
		if(!(file_exists($ruta))){
			$result = false;
		}
		return $result;
	}

	/**
	 * Check
	 *
	 * Metodo estatico que chequea si hay helpers o libs 
	 * para cargar de manera automatica, si los hay, crea 
	 * un objeto Autoload para que los cargue.
	 * Tambien chequea si la opcion de probar la carga de 
	 * clases/interfaces esta activada o no.
	 *
	 * @access public
	 */
	public static function check(){
		require_once("conf/autoload.php");
		if(!empty($helpers)){
			self::$existsHelpers = true;
		}
		if(!empty($libs)){
			self::$existsLibs = true;
		}
		if(self::$existsHelpers || self::$existsLibs){
			self::$tryLoad = ($tryLoad) ? true : false;
			new Autoload($helpers, $libs);
		}elseif($tryLoad){
			self::$tryLoad = $tryLoad;
			new Autoload();
		}
	}
}
// END Autoload Class

/* End of file Autoload.php */
/* Location: ./core/libs/Autoload.php */