<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase que dispara errores.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage Libs
 */

class ErrorHandler{

	/**
	 * Shot Error
	 *
	 * Invoca al modulo de error configurado.
	 *
	 * @access public
	 * @param string
	 * @param string
	 * @param string
	 * @param string (podria ser tambien un array)
	 */
	public static function shootError($errorType
									 ,$errorModule     = ERROR_MODULE
									 ,$errorController = ERROR_CONTROLLER
									 ,$errorMethod     = ERROR_METHOD
									 ,$args 		   = "Error sin definir"){

		if(ERROR_MSG && $errorType == ERROR_ROUTING){
			self::callError($errorModule, $errorController, $errorMethod, $args);
		}elseif(ERROR_FUNCTION && $errorType == ERROR_FUNC){
			self::callError($errorModule, $errorController, $errorMethod, $args);
		}
	}

	private static function callError($errorModule, $errorController, $errorMethod, $args){
		if(secure_import(APP_PATH."modules.{$errorModule}.controllers.{$errorController}")){
			$controller = build_controller_name($errorController);
			if(!is_array($args)){
				settype($args,'array');
			}
			if(class_exists($controller)){
				$er = new $controller($errorMethod,$args);
			}else{
				print("No se encontro la clase en el modulo de Error Especificado.");
			}
		}else{
			print("El modulo de error no esta definido o se definio de de manera incorrecta.");
		}
	}
}
// END ErrorHandler Class

/* End of file ErrorHandler.php */
/* Location: ./core/libs/ErrorHandler.php */