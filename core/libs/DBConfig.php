<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase que accede a la configuracion de acceso a la db.
 * 
 * No se carga por defecto. (VER SI SE PONE EN LA AUTOCARGA DE DARLE UNA INSTANCIA AL CONTROLLER)
 *
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage Libs
 */

class DBConfig{
	private $db = array();
	
	/**
	 * Constructor
	 */
	public function __construct(){
		$this->load();
	}

	/**
	 * Load
	 *
	 * Carga el archivo de configuracion para el acceso a la db.
	 *
	 * @access private
	 */
	private function load(){
		require_once("conf/db_conf.php");
		$this->db = $db;
	}

	/**
	 * Get
	 *
	 * Retorna el valor requerido de la configuracion.
	 *
	 * @access public
	 * @param string (ej. 'default')
	 * @param string (ej. 'hostname')
	 * @return string (valor de la propiedad de configuracion)
	 */
	public function get($index, $value){
		$item;
		if(array_key_exists($index, $this->db) && in_array($value, $this->db)){
			$item = $this->db[$index][$value];
		}else{
			$item = false;
		}
		return $item;
	}
}
// END DBConfig Class

/* End of file DBConfig.php */
/* Location: ./core/libs/DBConfig.php */