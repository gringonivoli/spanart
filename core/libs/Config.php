<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
import(CORE_PATH."interfaces.ConfigInterface");
/**
 * Clase que accede a los archivos de configuracion y a sus propiedades.
 * HACERLE UN METODO SET!
 *
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 * @subpackage Libs
 */

class Config implements ConfigInterface{
	
	private $config = array();

	/**
	 * Load
	 *
	 * Carga ficheros de configuracion creados por los usuarios 
	 * cuya caracteristica es que los valores de configuracion 
	 * deben estar contenidos en un diccionario clave => valor
	 *
	 * @access public
	 * @param string
	 * @param boolean
	 */
	public function load($file = '', $useSections = false){
		$file = "conf/{$file}.php";
		if(file_exists($file)){
			require_once($file);
			if($useSections){
				$this->config[$file] = array_merge($this->config[$file], $config);
			}else{
				$this->config = array_merge($this->config, $config);
			}
		}else{
			//ver de disparar un error o tirar u log..
		}
	}

	/**
	 * Item
	 *
	 * Retorna el valor de el item pasado como argumento 
	 * y acepta opcionalemnte un segundo parametro que es 
	 * para especificar la seccion, si cuando se cargo el 
	 * fichero de configuracion con Load se especifico el 
	 * uso de secciones con el valor true como segundo 
	 * argumento.
	 *
	 * @access public
	 * @param string
	 * @param string
	 * @return string/boolean (el valor del item o false si el item no existe)
	 */
	public function item($item = '', $index = ''){
		$value = false;
		if($index == ''){
			if(isset($this->config[$item])){
				$value = $this->config[$item];
			}
		}else{
			if(array_key_exists($index, $this->config) && in_array($item, $this->config)){
				$value = $this->config[$index][$item];
			}
		}
		return $value;
	}
}
// END Config Class

/* End of file Config.php */
/* Location: ./core/libs/Config.php */