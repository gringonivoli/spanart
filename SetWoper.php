<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase que inicializa la configuracion por defecto del woper-mvc.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 */

final class SetWoper{

	/**
	 * Constructor
	 *
	 * @access public
	 */
	public function __construct(){
		$this->setAppConf();
	}

	/**
	 * Set App Conf
	 *
	 * Setea todas las constantes necesarias para la app.
	 *
	 * @access private
	 */
	private function setAppConf(){
		require_once("conf/app_conf.php");
		$this->setPaths($woper_path, $static_path, $core_path, $app_path, $web_path);
		$this->setModules($init, $error);
		$this->setEnvironment($environment);
		$this->setErrorMsg($error_msg);
		$this->setErrorFunc($error_func);
		$this->setErrorsType();
	}

	/**
	 * Set Paths
	 *
	 * Setea las rutas necesarias para la app.
	 *
	 * @access private
	 */
	private function setPaths($woper_path ,$static_path, $core_path, $app_path, $web_path){
		$this->setWoperPath($woper_path);
		$this->setStaticPath($static_path);
		$this->setCorePath($core_path);
		$this->setAppPath($app_path);
		$this->setWebPath($web_path);
	}

	/**
	 * Set Modules
	 *
	 * Setea los modulos app.
	 *
	 * @access private
	 */
	private function setModules($init, $error){
		$this->setInit($init);
		$this->setError($error);
	}

	/**
	 * Set Init
	 * 
	 * Setea el modulo de inicio por defecto de la app.
	 *
	 * @access private
	 */
	private function setInit($init){
		define("INIT_MODULE", $init['module']);
		define("INIT_CONTROLLER", $init['controller']);
		define("INIT_METHOD", $init['method']);
	}

	/**
	 * Set Error.
	 *
	 * Setea el modulo de que usara la app.
	 *
	 * @access private
	 */
	private function setError($error){
		define("ERROR_MSG", $error['on']);
		define("ERROR_MODULE", $error['module']);
		define("ERROR_CONTROLLER", $error['controller']);
		define("ERROR_METHOD", $error['method']);
	}

	/**
	 * Set Error Func
	 *
	 * Setea si se utilizara los avisos de errores internos del motor.
	 *
	 * @access private
	 * @param boolean
	 */
	private function setErrorFunc($error_func){
		if(ENVIRONMENT == 'production'){
			$error_func = false;
		}
		define("ERROR_FUNCTION", $error_func);
	}

	/**
	 * Set Errors Type
	 *
	 * Se definen los tipos de errores internos del motor.
	 *
	 * @access private
	 */
	private function setErrorsType(){
		define("ERROR_ROUTING", "routing");
		define("ERROR_FUNC", "function");
	}

	/**
	 * Set Woper Path
	 *
	 * Setea la ruta completa en donde se ejecuta la app.
	 *
	 * @access private
	 */
	private function setWoperPath($woper){
		if($woper == ''){
			# CAMBIADO POR SEGURIDAD
			#$woper = str_replace('app_ignition.php', '', $_SERVER['SCRIPT_FILENAME']);
			$woper = dirname(__FILE__)."/";
		}
		define("WOPER_PATH", $woper);
		ini_set('include_path', WOPER_PATH);
	}

	/**
	 * Set Web Path
	 *
	 * Setea la ruta DESDE el dominio, sin incluir a este, en donde 
	 * se ejecuta la app.
	 *
	 * @access private
	 */
	private function setWebPath($web_path){
		if($web_path == ''){
			# CAMBIADO POR SEGURIDAD
			#$web_path = str_replace('app_ignition.php', '', $_SERVER['PHP_SELF']);
			#$web_path = ltrim($web_path, '/');
			$web_path = str_replace('app_ignition.php', '', $_SERVER['SCRIPT_NAME']);
			$web_path = ltrim($web_path, '/');
		}
		define("WEB_PATH",$web_path);
	}

	/**
	 * Set App Path
	 *
	 * Setea la ruta a el directorio de la app a desarrollar.
	 *
	 * @access private
	 */
	private function setAppPath($app_path){
		define("APP_PATH",$app_path);
	}

	/**
	 * Set Static Path
	 *
	 * Setea la ruta a el directorio del contenido estatico.
	 *
	 * @access private
	 */
	private function setStaticPath($static_path){
		define("STATIC_PATH",$static_path);
	}

	/**
	 * Set Core Path
	 *
	 * Setea la ruta a el directorio que contiene el core.
	 *
	 * @access private
	 */
	private function setCorePath($core_path){
		define("CORE_PATH",$core_path);
	}

	/**
	 * Set Environment
	 *
	 * Segun en entorno setea el nivel de error para php.
	 *
	 * @access private
	 */
	private function setEnvironment($default_envi){
		define("ENVIRONMENT",$default_envi);
		switch (ENVIRONMENT) {
			case 'development':
				ini_set('error_reporting', E_ALL | E_STRICT | E_NOTICE);
				ini_set('display_errors', 'On');
    			ini_set('track_errors', 'On');
				break;
			case 'production':
				ini_set('display_errors', 'Off');
				break;
			case 'testing':
				// Defina lo que considere necesario.
				break;
		}
	}

	/**
	 * Set Error Msg
	 *
	 * Setea los mensajes de errores frecuentes en el desarrollo con esta 
	 * herramienta.
	 *
	 * @access private
	 */
	private function setErrorMsg($default_msg){
		define("ERROR_MSG_NO_CLASS",$default_msg[ENVIRONMENT]['no_class']);
		define("ERROR_MSG_NO_METHOD",$default_msg[ENVIRONMENT]['no_method']);
		define("ERROR_MSG_NO_FILE",$default_msg[ENVIRONMENT]['no_file']);
	}
}
// END SetWoper Class

/* End of file SetWoper.php */
/* Location: ./SetWoper.php */