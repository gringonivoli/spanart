<?php
/**
 * Script que corre la aplicacion
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 */

/**
 * const RESTRICTED
 *
 * Definicion de una constante para restringir el acceso, 
 * una traba mas a intentos de acceso no esperados.
 * No cambiar!
 */
define("RESTRICTED",1);

/* 
| Carga y seteo de la configuracion de la aplicacion.
*/
require_once("SetWoper.php");
$setWoper = new SetWoper();

/*
| Carga del helper de importacion.
*/
require_once(CORE_PATH."helpers/import.php");

/*
| Carga de los archivos necesarios para el arranque.
*/
import(CORE_PATH."helpers.mvc");
import(CORE_PATH."helpers.uri");
import(CORE_PATH."libs.ErrorHandler");
import(CORE_PATH."libs.Autoload");
import(CORE_PATH."mvc.FrontController");
import(CORE_PATH."mvc.Request");
import(CORE_PATH."mvc.Armador");
import(CORE_PATH."mvc.Dispatcher");
import(CORE_PATH."mvc.Controller");
import(CORE_PATH."mvc.View");

/*
| Llamada al metodo estatico para comprobar si es 
| necesario la carga automatica de helpers, libs y/o el intento
| de resolucion de clases/interfaces no definidas.
*/
Autoload::check();

# VER QUE ONDA DE HACER MAS ADELANTE UN HOOK CHECK

/*
| Llamada al metodo estatico que inicia la aplicacion.
*/
FrontController::start();

/* End of file app_ignition.php */
/* Location: ./app_ignition.php */