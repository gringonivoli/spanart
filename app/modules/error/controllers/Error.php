<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase Controlador de errores (urls, metodos, archivos inexistentes, etc) basicos por defecto.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 */

class ErrorController extends Controller{
	private $message;
	
	public function error($message){
		$this->message = $message;
		print $this->message."<br><a href='/".WEB_PATH."'>Volver a la pagina principal</a>";
	}
}

/* End of file Error.php */
/* Location: ./error/Frontcontroller.php */