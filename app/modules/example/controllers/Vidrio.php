<?php
#import(DEFAULT_CORE_PATH."mvc.Controller");
#import(DEFAULT_APP_PATH."modules.example.models.Vidrio");
#import(DEFAULT_APP_PATH."modules.example.views.Vidrio");

class VidrioController extends Controller{
	/* Esto se puede poner o no, ya que si no se define 
	un constructor usara el del padre en su propio ambito 
	y todas las llamadas se ejecutaran en el objeto.
	
	function __construct($recurso='',$args=array(),$c,$m){
		parent::__construct($recurso,$args,$c,$m);
	}
	*/
	function index(){
		echo "esto es para probar el redireccionamiento automatico al metodo index cuando no se espesifica el metodo";
	}
	function ver($id=0,$color=''){
		//$vidrio = new Vidrio();
		$this->model->vidrio_id = $id;
		$this->model->color = $color;
		$this->model->get();

		#echo "estoy en el metodo?<br>";
		#print_r($this);
		//$view = new VidrioView();
		$this->view->ver($this->model);
	}
}