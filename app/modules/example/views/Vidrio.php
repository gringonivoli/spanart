<?php
# Vista: /woper-mvc/example/views/vidrio.php
class VidrioView{
	function ver($objeto=NULL){
		settype($objeto,'array');
		$comodines = array_keys($objeto);
		foreach($comodines as &$comodin){
			$comodin = "{".$comodin."}";
		}
		$valores = array_values($objeto);
		$template = file_get_contents(STATIC_PATH."html/ver_vidrio.html");
		print str_replace($comodines, $valores, $template);
	}

	function mostrar_form_alta(){
		$plantilla = file_get_contents(STATIC_PATH."html/agregar_vidrio.html");
		print $plantilla;
	}
}