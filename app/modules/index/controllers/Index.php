<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Clase Controlador de Inicio (index por defecto); ejemplifica la forma basica de crear tus 
 * controladores.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 */

//import(DEFAULT_CORE_PATH."mvc.Controller");

class IndexController extends Controller{
	/* Esto se puede poner o no, ya que si no se define 
	un constructor usara el del padre en su propio ambito 
	y todas las llamadas se ejecutaran en el objeto.
	
	public function __construct($recurso='',$args=array()){
		parent::__construct($recurso,$args);
	}
	*/
	public function index(){
		#print "JOJOJO ANDA LA PORONGA!!!!<br>";
		#echo "<a href='/woper-mvc02/example/vidrio'>Que onda?</a>";
		$this->view->prueba();
	}
}

/* End of file Index.php */
/* Location: ./index/Index.php */