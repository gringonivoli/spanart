<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");

class IndexView extends View{
	
	public function prueba(){
		#seteo un diccionario choto
		$p['COMENTARIO'] = "esto es un comentario por marca.. no por MARACA... JA!";
		#llamo a crear la vista estatica
		$render = $this->renderStatic($p, $this->getHtml('web/index'), true, true);
		#creo un objeto para acceder a la configuracion
		$c = new Config();
		#cargo los archivos de configuracion
		$c->load("prueba");
		#seteo un diccionario para el template
		$dict['{APP_TITLE}'] = $c->item('prueba');
		#muestro la pantalla
		$this->show('templates/default', $render, $dict);
	}
}