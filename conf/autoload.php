<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Archivos para especificar la carga de helpers/libs de 
 * manera automatica.
 * Y especificar el intento de carga de clases/interfaces
 * no definidas.
 * Tener en cuenta que utilizar cualquiera de estas opciones 
 * puede tener un costo en el rendimiento.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 */

/**
 * var try_load
 *
 * Para que de forma automatica se trata de cargar 
 * una clase/interfaz que no se definio con include, 
 * require_once, import, secure_import o en los arrays
 * de autocarga que se encuetran mas abajo.
 * Si se pone en true se busca en los directorios de 
 * libs, helpers, interfaces tanto del core como de la app.
 */
$tryLoad = false;

/**
 * var helpers
 *
 * Array en el que sus miembros son los helpers, de la app o del core, 
 * que se van a cargar de forma automatica para estar disponibles desde 
 * cualquier lugar de la app. (ej.: $helpers = array('helper1', 'helper2');).
 * No hace falta especificar si son de la app o del core, solo el nombre del 
 * fichero.
 */
$helpers = array('');

/**
 * var libs
 *
 * Array en el que sus miembros son las libs, de la app o del core, 
 * que se van a cargar de forma automatica para estar disponibles desde 
 * cualquier lugar de la app. (ej.: $libs = array('lib1', 'lib2');).
 * No hace falta especificar si son de la app o del core, solo el nombre del 
 * fichero.
 */
$libs = array('Config');

/* End of file autoload.php */
/* Location: ./conf/autoload.php */