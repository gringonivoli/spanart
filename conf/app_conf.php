<?php if(!defined("RESTRICTED")) exit("El script no permite el acceso directo.");
/**
 * Archivos de la configuracion gral. de la app.
 * 
 * @author Maxi Nivoli <maxi.nivoli@gmx.com>
 * @package Woper Engine
 */

/*********************************************************************\
|																	  |
| Configuracion de los Modulos de Inicio y Error.					  |
|														    		  |
\*********************************************************************/
/**
 * Configuracion del modulo de inicio.
 * Significado de las llaves del array:
 * "module" 	=> nombre de la carpeta.
 * "controller" => nombre del controlador.
 * "recurso"	=> nombre del metodo a invocar.
*/

$init = array(
				'module'	 =>'index'
			   ,'controller' =>'Index'
			   ,'method'	 =>'index');

/**
 * Configuracion para el modulo de error/ayuda del propio motor.
 * Significado de las llaves del array:
 * "on" 		=> true o false.
 * "module" 	=> nombre de la carpeta.
 * "controller" => nombre del controlador.
 * "recurso"	=> nombre del metodo a invocar.
*/

$error = array(
				 'on'		=> true
				,'module'	=> 'error'
				,'controller'=> 'Error'
				,'method'	=> 'error');

/*********************************************************************\
|																	  |
| Configuracion de Errores Funcionales Internos.					  |
|														    		  |
\*********************************************************************/
/**
 * Configuracion para activar el error interno del motor que puede 
 * ser de ayuda para un evironment = development, si el environment 
 * se define como production, por defecto este valor sera false.
 */
$error_func = true;

/*********************************************************************\
|																	  |
| Configuracion de Rutas.  											  |
|														    		  |
\*********************************************************************/
/**
 * Configuracion para el directorio de woper.
 * Si no se sabe bien que poner dejar vacio, 
 * woper intentara, y posiblemente lo logre, 
 * averiguar la configuracion necesaria. 
*/

$woper_path = "";
/**
 * Configuracion para la ruta de la app desde el dominio.
 * Si la app esta en el directorio raiz del dominio dejar vacio.
 * Si no se sabe bien que poner tambien dejar vacio, 
 * woper intentara, y posiblemente logre, 
 * averiguar la configuracion necesaria. 
*/

$web_path = "";
/**
 * Configuracion para el directorio de la app.
*/
$app_path = "app/";

/**
 * Configuracion de la ruta del nucleo por defecto.
*/

$core_path = "core/";

/**
 * Configuracion de la ruta de de contenido estatico por defecto.
*/

$static_path = "static/";

/*********************************************************************\
|																	  |
| Configuracion de Entorno y sus Mensajes.							  |
|														    		  |
\*********************************************************************/
/**
 * Configuracion del ENVIRONMENT, development, testing, production.
*/

$environment = "development";

/**
 * Configuracion de algunos mensajes por defecto de los errores que puede arrojar el motor, 
 * hasta el momento, estos sirven de ayuda para un debug mas rapido, estan discriminado por 
 * el tipo de ENVIRONMENT en que se encuentre la app. 
 * Los tres primeros estan relacionados con las llamadas a controladores, se pueden usar para
 * mostrar un mensaje de pagina no encontrada con un link al inicio por defecto, de hecho, esta 
 * es la manera en que estan implementados por defecto.
*/

$error_msg['development'] = array(
							 'no_class' => 'P&aacute;gina no encontrada - Problema: clase perdida.'
							,'no_method'=> 'P&aacute;gina no encontrada - Problema: m&eacute;todo perdido.'
							,'no_file'  => 'P&aacute;gina no encontrada - Problema: fichero/directorio perdido.');

$error_msg['production'] = array(
							 'no_class' => 'P&aacute;gina no encontrada.'
							,'no_method'=> 'P&aacute;gina no encontrada.'
							,'no_file'  => 'P&aacute;gina no encontrada.');

$error_msg['testing'] = array(
							 'no_class' => ''
							,'no_method'=> ''
							,'no_file'  => '');

/* End of file app_conf.php */
/* Location: ./conf/app_conf.php */